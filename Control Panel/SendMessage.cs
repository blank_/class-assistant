﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Panel
{
    public partial class SendAdminMessage : Form
    {
        public SendAdminMessage()
        {
            InitializeComponent();
        }

        public string message = "";
        public bool ok = false;

        private void button1_Click(object sender, EventArgs e)
        {
            message = textBox1.Text;
            ok = true;
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int message, int lparam, int wparam);

        [DllImport("user32.dll")]
        public static extern void ReleaseCapture();

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0xA1, 0x2, 0);
        }
    }
}
