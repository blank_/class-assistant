﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_Panel
{
    public partial class TogglleTaskManager : Form
    {
        public string enable = "";
        public bool ok = false;
        public TogglleTaskManager()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            enable="enable";
            ok = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        { 
            enable = "disable";
            ok = true;
            this.Close();
        }

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int message, int lparam, int wparam);

        [DllImport("user32.dll")]
        public static extern void ReleaseCapture();

        private void label2_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, 0xA1, 0x2, 0);
        }
    }
}
