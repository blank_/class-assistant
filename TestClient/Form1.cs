﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //code copied from here:https://stackoverflow.com/questions/7268302/get-the-titles-of-all-open-windows
        //and here: https://stackoverflow.com/questions/17868453/find-and-close-multiple-windows
        public class InfoWindow
        {
            public IntPtr Handle = IntPtr.Zero;
            //public FileInfo File = new FileInfo(Application.ExecutablePath);
            public string Title = Application.ProductName;
            public string state="Unknown";

            public override string ToString()
            {
                return Title;
            }
        }//CLASS

        /// <summary>Returns a dictionary that contains the handle and title of all the open windows.</summary>
        /// <returns>A dictionary that contains the handle and title of all the open windows.</returns>
        public static IDictionary<IntPtr, InfoWindow> GetOpenedWindows()
        {
            IntPtr shellWindow = GetShellWindow();
            Dictionary<IntPtr, InfoWindow> windows = new Dictionary<IntPtr, InfoWindow>();

            EnumWindows(new EnumWindowsProc(
                delegate (IntPtr hWnd, int lParam)
                {
                    if (hWnd == shellWindow) return true;
                    if (!IsWindowVisible(hWnd)) return true;
                    int length = GetWindowTextLength(hWnd);
                    if (length == 0) return true;
                    StringBuilder builder = new StringBuilder(length);
                    GetWindowText(hWnd, builder, length + 1);
                    var info = new InfoWindow();
                    info.Handle = hWnd;
                    //info.File = new FileInfo(GetProcessPath(hWnd));
                    info.Title = builder.ToString();
                    WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                    GetWindowPlacement(hWnd, ref placement);
                    switch (placement.showCmd)
                    {
                        case 1:
                            info.state = "Normal";
                            break;
                        case 2:
                            info.state = "Minimized";
                            break;
                        case 3:
                            info.state = "Maximized";
                            break;
                    }

                    windows[hWnd] = info;
                    return true;
                }), 0);
            return windows;
        }


        //https://stackoverflow.com/questions/16610567/enable-disable-taskmanager
        public void SetTaskManager(bool enable)
        {
            RegistryKey objRegistryKey = Registry.CurrentUser.CreateSubKey(
                @"Software\Microsoft\Windows\CurrentVersion\Policies\System");
            if (enable && objRegistryKey.GetValue("DisableTaskMgr") != null)
                objRegistryKey.DeleteValue("DisableTaskMgr");
            else
                objRegistryKey.SetValue("DisableTaskMgr", "1");
            objRegistryKey.Close();
        }

        bool TaskManagerEnabled()
        {
            RegistryKey objRegistryKey = Registry.CurrentUser.CreateSubKey(
               @"Software\Microsoft\Windows\CurrentVersion\Policies\System");

            return objRegistryKey.GetValue("DisableTaskMgr") != null;
            objRegistryKey.Close();
        }

        public static string GetProcessPathOld(IntPtr hwnd)
        {
            uint pid = 0;
            GetWindowThreadProcessId(hwnd, out pid);
            if (hwnd != IntPtr.Zero)
            {
                if (pid != 0)
                {
                    var process = Process.GetProcessById((int)pid);
                    if (process != null)
                    {
                        return process.MainModule.FileName.ToString();
                    }
                }
            }
            return "";
        }

        private delegate bool EnumWindowsProc(IntPtr hWnd, int lParam);

        //https://stackoverflow.com/questions/1003073/how-to-check-whether-another-app-is-minimized-or-not





        private static readonly Socket ClientSocket = new Socket
            (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        static byte[] buffer = new byte[2048];

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out uint processId);

        [DllImport("USER32.DLL")]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);
        [DllImport("USER32.DLL")]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("USER32.DLL")]
        private static extern IntPtr GetShellWindow();

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("USER32.DLL")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowTextLength(IntPtr hWnd);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool SetForegroundWindow(IntPtr windowHandle);

        uint WM_CLOSE = 0x10;
        private const int SW_MAXIMIZE = 3;
        private const int SW_MINIMIZE = 6;
        private const int SW_RESTORE = 9;


        private struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }

        

        private void ConnectToServer()
        {


            









            int attempts = 0;

            while (!ClientSocket.Connected)
            {
                try
                {
                    attempts++;

                    this.Invoke((Action)(() => showtoast("Class Assistant", "Connection attempt " + attempts, ToastNotifications.Notification.NotificationType.Info, 2)));
                    
                    ClientSocket.Connect(IPAddress.Loopback, 6161);
                }
                catch (SocketException ex)
                {
                    this.Invoke((Action)(() => showtoast("Class Assistant", ex.Message, ToastNotifications.Notification.NotificationType.Error, 2)));
                    
                }
            }
            
            this.Invoke((Action)(() => showtoast("Class Assistant", "Successfully connected to server!", ToastNotifications.Notification.NotificationType.Success, 2)));
            
        }
        

        private void Form1_Load(object sender, EventArgs e)
        {
            
            this.Visible = false;
            var thread = new Thread(
                        () =>
                        {
                            ConnectToServer();
                            ClientSocket.BeginReceive(buffer, 0, 2048, SocketFlags.None, ReceiveCallback, ClientSocket);
                            timer1.Start();
                        });
            thread.Start();

            
            

        }
        void showtoast(string title, string body, ToastNotifications.Notification.NotificationType type, int duration)
        {

            ToastNotifications.Notification n = new ToastNotifications.Notification(title, body, type, 7, ToastNotifications.FormAnimator.AnimationMethod.Slide, ToastNotifications.FormAnimator.AnimationDirection.Left);
            n.Show();
        }

        

        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;

            int received;

            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException)
            {
                Console.WriteLine("Client forcefully disconnected");
                // Don't shutdown because the socket may be disposed and its disconnected anyway.
                current.Close();

                return;
            }

            byte[] recBuf = new byte[received];
            Array.Copy(buffer, recBuf, received);
            string text = Encoding.Unicode.GetString(recBuf);

            try
            {
                if (text == "<screenshot>")
                {
                    using (Image r = takeScreenShot())
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            // Convert Image to byte[]
                            r.Save(ms, ImageFormat.Jpeg);
                            byte[] imageBytes = ms.ToArray();

                            // Convert byte[] to Base64 String
                            string base64String = Convert.ToBase64String(imageBytes);
                            byte[] datasend = Encoding.Unicode.GetBytes("<screenshot>" + base64String);
                            ClientSocket.Send(datasend);
                            //Clipboard.SetText(base64String);

                        }
                    }
                }



                else if (text.StartsWith("<message>"))
                {
                    this.Invoke((Action)(() => showtoast("Admin Message", text.Substring(9), ToastNotifications.Notification.NotificationType.Info, 7)));
                    
                }

                else if (text.StartsWith("<success>"))
                {
                    this.Invoke((Action)(() => showtoast("Class Assistant", text.Substring(9), ToastNotifications.Notification.NotificationType.Success, 7)));

                }

                else if (text.StartsWith("<window-minimize>"))
                {
                    ShowWindow(new IntPtr(int.Parse(text.Substring(17))), SW_MINIMIZE);
                }
                else if (text.StartsWith("<window-maximize>"))
                {
                    ShowWindow(new IntPtr(int.Parse(text.Substring(17))), SW_MAXIMIZE);
                }
                else if (text.StartsWith("<window-restore>"))
                {
                    ShowWindow(new IntPtr(int.Parse(text.Substring(16))), SW_RESTORE);
                }
                else if (text.StartsWith("<window-close>"))
                {
                    SendMessage(new IntPtr(int.Parse(text.Substring(14))), WM_CLOSE, 0, 0);
                }
                else if (text.StartsWith("<link>"))
                {
                    string link = text.Substring(6);
                    if (!link.StartsWith("http://") && !link.StartsWith("https://"))
                        link = "http://" + link;
                    Process.Start(link);
                }

                else if (text.StartsWith("<cmd-ui>"))
                {

                    string cmd = text.Substring(8);
                    Process.Start("cmd.exe", "/C "+cmd);
                }
                else if(text.StartsWith("<sid>"))
                {
                    byte[] data = Encoding.Unicode.GetBytes("<sid>" + WindowsIdentity.GetCurrent().User.ToString());
                    ClientSocket.Send(data);
                }
                else if (text.StartsWith("<taskmgr>"))
                {
                    byte[] data = Encoding.Unicode.GetBytes("<taskmgr>"+TaskManagerEnabled().ToString());
                    ClientSocket.Send(data);
                }
                
                else if (text.StartsWith("<windows>"))
                {
                    string text2 = "<windows>";
                    bool first = true;
                    IDictionary<IntPtr, InfoWindow> windows = GetOpenedWindows();
                    foreach (KeyValuePair<IntPtr, InfoWindow> window in windows)
                    {
                        if (!first)
                        {
                            text2 += "`";
                        }
                        else first = false;
                        text2 += window.Value.Handle + "~" + window.Value.Title + "~" + window.Value.state;


                    }
                    byte[] data = Encoding.Unicode.GetBytes(text2);
                    ClientSocket.Send(data);
                }
                else
                {
                    this.Invoke((Action)(() => showtoast("Class Assistant", text, ToastNotifications.Notification.NotificationType.Info, 7)));
                    

                }
            }
            catch(Exception ex)
            {
                byte[] data = Encoding.Unicode.GetBytes("<error>"+ex.Message);
                ClientSocket.Send(data);
            }
            

            ClientSocket.BeginReceive(buffer, 0, 2048, SocketFlags.None, ReceiveCallback, ClientSocket);
        }

        string currentwindow()
        {
            int textLength = GetWindowTextLength(GetForegroundWindow());
            StringBuilder outText = new StringBuilder(textLength + 1);
            int a = GetWindowText(GetForegroundWindow(), outText, outText.Capacity);
            return outText.ToString();
        }

        private static Image takeScreenShot()
        {

            Bitmap b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            using (Graphics g = Graphics.FromImage(b))
            {
                g.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size);
                return b;
            }

        }
        string window = "";

        private void timer1_Tick(object sender, EventArgs e)
        {
            string currentwnd = currentwindow();
            if(window!=currentwnd)
            {
                window = currentwnd;
                byte[] data = Encoding.Unicode.GetBytes("<window>" + currentwnd);
                ClientSocket.Send(data);
            }
            
        }

        private void Form1_VisibleChanged(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void sendMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var f = new SendUserMessage())
            {
                f.ShowDialog();
                if (f.ok)
                {
                    sendmessage("<message>" + f.message);
                }
            }
        }
        void sendmessage(string message)
        {
            byte[] data = Encoding.Unicode.GetBytes(message);
            try
            {
                ClientSocket.Send(data);
            }
            catch(Exception ex)
            {

                this.Invoke((Action)(() => showtoast("Error talking to service", ex.Message, ToastNotifications.Notification.NotificationType.Error, 7)));
                
            }
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.WindowsShutDown) sendmessage("<allow-close>");
            else e.Cancel = true;

        }

        private void sendScreenShotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Image r = takeScreenShot())
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    r.Save(ms, ImageFormat.Jpeg);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    byte[] datasend = Encoding.Unicode.GetBytes("<screenshot>" + base64String);
                    ClientSocket.Send(datasend);
                    //Clipboard.SetText(base64String);

                }
            }
        }
    }
}
